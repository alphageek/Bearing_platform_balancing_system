#include"reg52.h"
#include"math.h"
#include"stdio.h"
#include"intrins.h"
#define uchar unsigned char
#define uint unsigned int
#define DataPort P0		   //LCD1602数据端口 
sbit SCL=P2^1;			   //IIC时钟引脚定义 
sbit SDA=P2^0;				//IIC时钟引脚定义 

sbit ENA=P1^4;
sbit IN1=P1^2;
sbit IN2=P1^3;
sbit ENB=P1^5;
sbit IN3=P1^6;
sbit IN4=P1^7;

sbit KEY1 = P3^4;
sbit KEY2 = P3^5;
sbit KEY3 = P3^6;
sbit KEY4 = P3^7;

float Roll,Pitch;
uchar x = 0,y = 0,sflag;
uchar xflag,yflag;
uint ge_x,ge_y,shi_x,shi_y;

sbit LCM_RS=P1^0;		   //LCD1602命令端口 
sbit LCM_RW=P1^1;
sbit LCM_EN=P2^5;

#define SlaveAddress 0xA6
							 
typedef unsigned char BYTE;
typedef unsigned short WORD;

BYTE BUf[8];
unsigned char code disp_code[]={"0123456789"};
uchar ge,shi,bai,qian,wan;
int dis_data;
int data_xyz[3];
void delay(unsigned int k);
void InitLcd();
void Init_ADXL345(void);

void WriteDataLCM(uchar dataW);
void WriteCommandLCM(uchar CMD,uchar Attribc);
void DisplayOneChar(uchar X,uchar Y,uchar DData);
void conversion(uint temp_data);

void Single_Write_ADXL345(uchar REG_Address,uchar REG_data);
uchar Single_Read_ADXL345(uchar REG_Address);
void Multiple_Read_ADXL345();

void jiaodu_init();
void jiaodu_xianshi();
void LCD1602(uchar,uchar,uchar);

void dianji_init();
void dianji_qudong();

void Key_Scan();
void Delay5us();
void Delay5ms();

void ADXL345_Start();
void ADXL345_Stop();
void ADXL345_SendAck(bit ack);

bit ADXL345_RecvAck();
void ADXL345_SendByte(BYTE dat);
BYTE ADXL345_RecvByte();
void ADXL345_ReadPage();
void ADXL345_WritePage();

void conversion(uint temp_data)
{
	wan=temp_data/10000+0x30;
	temp_data=temp_data%10000;
	qian=temp_data/1000+0x30;
	temp_data=temp_data%1000;
	bai=temp_data/100+0x30;
	temp_data=temp_data%100;
	shi=temp_data/10+0x30;	 
	temp_data=temp_data%10;	   
	ge=temp_data+0x30;

}
void Key_Scan()
{
	if(KEY1 == 0)
	{
		Delay5us();
		if(xflag == 1)
		{	 
			x=x+1;
			if(x > 60)
				x = 0;
			while(!KEY1);
		}
		if(yflag == 1)
		{	
			y=y+1;
			if(y > 60)
				y = 0;
		   while(!KEY1);
		}
	}


	if(KEY2 == 0)
	{
  		Delay5us();
		if(xflag == 1)
		{
		   	x=x-1;
			if ( 0 == x)
				x = 0;
			while(!KEY2);
		}

		if(yflag == 1)
		{
			y=y-1;
			if ( 0 == y)
				y = 0 ;
			while(!KEY2);
		}
	}


	if(KEY3 == 0)
	{	
		
		int i;
		xflag = 0;
		yflag = 1;
		sflag = 0;
		for( i=0;i<50;i++)
		Delay5ms();
	
		if(KEY3 == 0)
		{
	//	Delay5us();
		xflag = 1;
		yflag = 0;
		
		while(!KEY3);
		} 
	 
		while(!KEY3);
	}
	if(KEY4 == 0)
		{
		Delay5us();
		sflag = 1;
		while(!KEY4);
			}	
}		
void delay(unsigned int k)
{
	unsigned int i,j;
	for(i=0;i<k;i++)
		for(j=0;j<121;j++)
		{;}
}
void WaitForEnable(void)
{
	DataPort=0xff;
	do
	{
	LCM_RS=0;
	LCM_RW=1;
	_nop_();
	LCM_EN=0;
	LCM_EN=1;
	_nop_();
	_nop_();
	}
	while(DataPort&0x80);
	LCM_EN=0;
	
}
void WriteCommandLCM(uchar CMD,uchar Attribc)
{	
		if(Attribc)
			WaitForEnable();
		LCM_RS=0;
		LCM_RW=0;
		_nop_();
		DataPort=CMD;
		_nop_();
		LCM_EN=1;
		_nop_();
		_nop_();
		LCM_EN=0;
}
void WriteDataLCM(uchar dataW)
{
	WaitForEnable();
	LCM_RS=1;
	LCM_RW=0;
	_nop_();
	DataPort=dataW;
	_nop_();
	LCM_EN=1;
	_nop_();
	_nop_();
	LCM_EN=0;
}
void InitLcd()
{
	WriteCommandLCM(0x38,1);
	WriteCommandLCM(0x08,1);
	WriteCommandLCM(0x01,1);
	WriteCommandLCM(0x06,1);
	WriteCommandLCM(0x0c,1);
}
void DisplayOneChar(uchar X,uchar Y,uchar DData)
{
	Y&=1;
	X&=15;
	if(Y)
		X|=0x40;
	X|=0x80;
	WriteCommandLCM(X,0);
	WriteDataLCM(DData);
}
void Delay5us()
{
	_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();
	_nop_();_nop_();_nop_();_nop_();
}
void Delay5ms()
{
	WORD n=560;
	while(n--);
}

void ADXL345_Start()
{
	SDA=1;
	SCL=1;
	Delay5us();
	SDA=0;
	Delay5us();
	SCL=0;
}

void ADXL345_Stop()
{
	SDA=0;
	SCL=1;
	Delay5us();
	SDA=1;
	Delay5us();
}

void ADXL345_SendAck(bit ack)
{
	SDA=ack;
	SCL=1;
	Delay5us();
	SCL=0;
	Delay5us();
}

bit ADXL345_RecvAck()
{
	SCL=1;
	Delay5us();
	CY=SDA;
	SCL=0;
	Delay5us();
	return CY;
}

void ADXL345_SendByte(BYTE dat)
{
	BYTE i;
	for(i=0;i<8;i++)
	{
		dat<<=1;
		SDA=CY;
		SCL=1;
		Delay5us();
		SCL=0;
		Delay5us();
	}
	ADXL345_RecvAck();
}

BYTE ADXL345_RecvByte()
{
	BYTE i;
	BYTE dat=0;
	SDA=1;
	for(i=0;i<8;i++)
	{
		dat<<=1;
		SCL=1;
		Delay5us();
		dat|=SDA;
		SCL=0;
		Delay5us();
	}
	return dat;
}

void Single_Write_ADXL345(unsigned char REG_Address,unsigned char REG_data)
{
	ADXL345_Start();
	ADXL345_SendByte(SlaveAddress);
	ADXL345_SendByte(REG_Address);
	ADXL345_SendByte(REG_data);
	ADXL345_Stop();
}

unsigned char Single_Read_ADXL345(unsigned char REG_Address)
{
 	char REG_data;
	ADXL345_Start();
	ADXL345_SendByte(SlaveAddress);
	ADXL345_SendByte(REG_Address);
	ADXL345_Start();
	ADXL345_SendByte(SlaveAddress+1);
	REG_data=ADXL345_RecvByte();
	ADXL345_SendAck(1);
	ADXL345_Stop();
	return REG_data;
}

void Multiple_Read_ADXL345()
{
	char i;
	ADXL345_Start();
	ADXL345_SendByte(SlaveAddress);
	ADXL345_SendByte(0x32);
	ADXL345_Start();
	ADXL345_SendByte(SlaveAddress+1);
	for(i=0;i<6;i++)
	{
		BUf[i]=ADXL345_RecvByte();
		if(i==5)
		{
			ADXL345_SendAck(1);
		}
		else
		{
			ADXL345_SendAck(0);
		}
	}
	ADXL345_Stop();
	Delay5ms();
}

void Init_ADXL345()
{
	Single_Write_ADXL345(0x31,0x0B);
	Single_Write_ADXL345(0x2C,0x08);
	Single_Write_ADXL345(0x2D,0x08);
	Single_Write_ADXL345(0x2E,0x80);
	Single_Write_ADXL345(0x1E,0x00);
	Single_Write_ADXL345(0x1F,0x00);
	Single_Write_ADXL345(0x20,0x05);
}
void jiaodu_init()
{
		float Q,T,K;
  		Init_ADXL345();	  //初始化ADXL345
		Multiple_Read_ADXL345();  //连续读出数据，存储在BUf中
		data_xyz[0]=(BUf[1]<<8)+BUf[0];//合成数据
		data_xyz[1]=(BUf[3]<<8)+BUf[2];
		data_xyz[2]=(BUf[5]<<8)+BUf[4];
		//分别为加速度XYZ的原始数据，10位的
		Q=(float)data_xyz[0]*3.9;
		T=(float)data_xyz[1]*3.9;
		K=(float)data_xyz[2]*3.9;
	//	Q=-Q;
		Roll=(float)(((atan2(K,Q)*180)/3.1416)+180);//X轴角度
		Pitch=(float)(((atan2(T,Q)*180)/3.1416)+180);//Y轴角度	
}

void  jiaodu_xianshi()	
{
		conversion(Roll);	
		DisplayOneChar(2,1,'X');
		DisplayOneChar(3,1,':');
		DisplayOneChar(4,1,bai);
		DisplayOneChar(5,1,shi);
		DisplayOneChar(6,1,ge);
		conversion(Pitch);
		DisplayOneChar(2,2,'Y');
		DisplayOneChar(3,2,':');
		DisplayOneChar(4,2,bai);
		DisplayOneChar(5,2,shi);
		DisplayOneChar(6,2,ge);
		delay(200);		//延时		   
}

void LCD1602(uchar xx, uchar yy, uchar ss)
{	
		 ge_x = xx%10;
		 ge_y = yy%10;
		 shi_x =xx/10; 
		 shi_y = yy/10;
		
		DisplayOneChar(14,1,disp_code[ss]);
		DisplayOneChar(9,1,'X');
		DisplayOneChar(9,2,'Y');
		DisplayOneChar(10,1,':');
		DisplayOneChar(10,2,':');
  		DisplayOneChar(12,1,disp_code[ge_x]);
		DisplayOneChar(12,2,disp_code[ge_y]);
		DisplayOneChar(11,1,disp_code[shi_x]);
		DisplayOneChar(11,2,disp_code[shi_y]);		
			
}

void dianji_init()
{
	ENA=0;
	IN1=0;
	IN2=0;
	ENB=0;
	IN3=0;
	IN4=0;		
}

void dianji_qudong()
{
	if(0 == sflag)
	{
		ENA = 0;
		ENB = 0;
	}
	else if(1 == sflag)
	{
		ENA = 1;
		ENB = 1;
		conversion(Roll);
		if((ge_x+shi_x*10) <= ((ge - 0x30)+(shi-0x30)*10+1)&&(ge_x+shi_x*10) >= ((ge - 0x30)+(shi-0x30)*10-1))
				ENA = 0;
		 	else if((ge_x+shi_x*10) < ((ge - 0x30)+(shi-0x30)*10))
			{	
				IN1=1;
				IN2=0;
					
			}
			else if((ge_x+shi_x*10) > ((ge - 0x30)+(shi-0x30)*10))
			{	
				IN1=0;
				IN2=1;	
			}	 
		
		conversion(Pitch);
			if((ge_y+shi_y*10) <= ((ge - 0x30)+(shi-0x30)*10+1)&&(ge_y+shi_y*10) >= ((ge - 0x30)+(shi-0x30)*10-1))
				ENB = 0;
		 	else if((ge_y+shi_y*10) < ((ge - 0x30)+(shi-0x30)*10))
			{
				IN3=1;
				IN4=0;
			}
			else if((ge_y+shi_y*10) > ((ge - 0x30)+(shi-0x30)*10))
			{
			 IN3=0;
			 IN4=1;
			} 
		} 
		
}

void over()
{		
	conversion(Roll);
	if((((ge - 0x30)+(shi-0x30)*10)+(bai - 0x30)*100 <= 360) &&(((ge - 0x30)+(shi-0x30)*10)+(bai - 0x30)*100 > 300))
	{			
		IN1=0;
		IN2=1;		
	}	
	conversion(Pitch);
	if((((ge - 0x30)+(shi-0x30)*10)+(bai - 0x30)*100 <= 360) &&(((ge - 0x30)+(shi-0x30)*10)+(bai - 0x30)*100 > 300))
	{
				IN3=0;
				IN4=1;	
	} 

}
void main()
{
	uchar devid;
	delay(500);
	InitLcd();		 //液晶初始化ADXL345 
	dianji_init();
	Init_ADXL345();
	devid=Single_Read_ADXL345(0x00); 
	while(1)
	{
		jiaodu_init();
		jiaodu_xianshi();
		LCD1602(x,y,sflag);
		Key_Scan();
		dianji_qudong();
		over();	
	}
}