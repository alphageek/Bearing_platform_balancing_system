 #if 0
 #include<reg51.h>   // 
 sbit P1_0 = P1^0;   // PWM  脉冲输出
 sbit key10 = P3^4;
 sbit key11 = P3^5;
 unsigned char Set_PWM0 = 1; // 占空比调整
 unsigned char counter = 0;  // 计数的
 bit bdata tt1;      // 标志位
 void main()
 {
 TMOD=0x01;
 TH0=(65536-500)/256;      // 定时时间  可以修改
 TL0=(65536-500)%256; 
 ET0=1;                                                                                                         
 EA=1;
 TR0=1;
 while(1)
 {                          // 开关调整 PWM  占空比
 if(key10==0 && tt1==0) 
 	{
 	tt1 = 1; 
	Set_PWM0+=1;
 	} // K1、K2
 if(key11==0 && tt1==0)
 	{
 	tt1 = 1; 
	Set_PWM0--;
	} 
 if(key10==1 && key11==1) 
 	tt1=0; 
 }         
 }
 void Timer0(void) interrupt 1 // 定时器0   PWM 控制                           
 {                            
   TH0=(65536-500)/256;       // 定时时间  可以修改
   TL0=(65536-500)%256;
   counter++;
   if(counter >= 16) 
   		P1_0=0;/*counter=0*/;   // PWM  16级  可以修改
   if(counter >= Set_PWM0) 
  		P1_0 = 0;
   else 
   		P1_0 = 1;
 }