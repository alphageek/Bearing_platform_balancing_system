#include <reg52.h>
unsigned char count;			//0.5ms次数标识
sbit pwm=P1^0;				//信号输出
sbit jia=P3^4;				//角度增加按键
sbit jan=P3^5;				//角度减小按键
char jd;					//角度标识
void delay(unsigned int i)
{
	unsigned int j,k;
	for(j=i;j>0;j--)
		for(k=125;k>0;k--);
}
void Time0_init()			//定时器初始化
{
	TMOD=0x01;
	IE=0x82;
	TH0=0xff;
	TL0=0x19;				//12晶振，0.25ms
	TR0=1;				   //定时器开始
}
void Time0_int() interrupt 1			//中断程序
{
	TH0=0xff;
	TL0=0x19;
	if(count<jd) //输出count个高电平
		pwm=1;
	else
	    pwm=0;    //其余输出低电平
	count++;
//	count=count%40;  //时钟保持40个count 即20ms
}
void keyscan()				//按键扫描
{
	if(jia==0)				//角度增加键是否按下
	{
		delay(50);			//按下延时，消抖
		if(jia==0)
		{
			jd++;			//角度标识加1
			count=0;		//按键按下，则20ms周期重新开始
			if(jd==50)
				jd=9;			 //已经是180°则保持
				while(jia==0);	 //等待按键放开
		}
	}
	if(jan==0)					 //角度减小键是否按下
	{
		delay(10);
		if(jd==0);
		{
			jd--;				 //角度标识减1
			count=0;
			if(jd==0)
				jd=1;
			while(jan==0);
		}
	}
}

void main()
{
	jd=4;
	//count=0;
	Time0_init();
	while(1)
	{
		keyscan();
//		display();
	}
}